# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
#
# $Id: SQZ_LO_LR.py $
# $HeadURL: https://redoubt.ligo-wa.caltech.edu/svn/cds_user_apps/trunk/sqz/h1/guardian/SQZ_LO.py $

import sys
import time
from guardian import GuardState, GuardStateDecorator
import cdsutils as cdu

nominal = 'LOCKED_OMC'
request = 'DOWN'

######FUNCTIONS######
'''
def LO_LOCKED():
    log('In LO_LOCKED()')
    flag = False
    RFave = cdu.avg(1,'H1:SQZ-HD_DIFF_RF3_DEMOD_RFMON', stddev = True) #vx 1/12
    if RFave[0] > -5 and RFave[1] < 0.1:   #vx 1/12
        flag = True
    return flag
'''

def LO_RAILED():
    return ( (abs(ezca['SQZ-LO_SERVO_FASTMON'])>9.5) or (abs(ezca['SQZ-LO_SERVO_SLOWMON'])>9.5) )

def TTFSS_LOCKED():
    if ezca['SQZ-FIBR_LOCK_STATUS_LOCKED'] == 1:
        if (abs(ezca['SQZ-FIBR_LOCK_BEAT_FREQUENCY']-160)<0.01): #Added extra logic to check whether squeezer beat frequency is correct 8/12/22 DG
            return True
    if ezca.read('GRD-SQZ_FC_STATE_S', as_string=True) == 'IR_VCO_LOCKED':
        return True

def OPO_LOCKED():
    if ezca.read('GRD-SQZ_OPO_LR_STATE_S', as_string=True) == 'LOCKED_CLF_DUAL' or ezca.read('GRD-SQZ_OPO_LR_STATE_S', as_string=True) == 'LOCKED_CLF_DUAL_NO_ISS' :
        return True

def CLF_locked():
    return (ezca['SQZ-CLF_VCXO_CONTROLS_DIFFFREQUENCY'] < 5) and (ezca['SQZ-CLF_VCXO_CONTROLS_DIFFFREQUENCY'] > -5)

def FC_LOCKED():
    flag = False
    if ezca['GRD-SQZ_FC_STATE_N'] >= 10:
        flag = True
    elif ezca.read('GRD-SQZ_FC_STATE_S', as_string=True) == 'FC_MISALIGNED':
        flag = True
    return flag

'''def IMC_LOCKED():
    #log('Checking IMC in IMC_LOCKED()')
    flag = False
    if ezca['GRD-IMC_LOCK_STATE_N'] == 100 or ezca['GRD-IMC_LOCK_STATE_N'] == 70:
        flag = True
    if ezca['GRD-IMC_LOCK_STATE_N'] == 55:
        flag = True
    return flag'''

#####################
class TTFSS_checker(GuardStateDecorator):
    def pre_exec(self):
        if not TTFSS_LOCKED():
            notify('TTFSS not locked')
            return 'DOWN'

class LO_RAILED_checker(GuardStateDecorator):
    def pre_exec(self):
        if LO_RAILED():
            notify('LO SERVO RAILING!')
#####################

class INIT(GuardState):
    index = 0
    def main(self):
        return True

class DOWN(GuardState):
    index = 1
    goto = True

    def main(self):
        ezca['SQZ-LO_SERVO_COMBOOST'] = 0
        ezca['SQZ-LO_SERVO_IN2EN'] = 0
        ezca['SQZ-LO_SERVO_IN1EN'] = 0
        ezca['SQZ-LO_SERVO_SLOWBOOST'] = 0
        ezca['SQZ-LO_SERVO_SLOWCOMP'] = 0
        ezca['SQZ-LO_SERVO_SLOWFILTER'] = 0
        ezca['SQZ-LO_SERVO_FASTEN'] = 1
        ezca['SQZ-LO_SERVO_SLOWOPT'] = 1
        ezca['SQZ-LASER_VCO_CONTROLS_EXTERNAL'] = 0
        ezca['SQZ-LASER_VCO_CONTROLS_ENABLE'] = 1
        #ezca['SQZ-LO_SERVO_IN1GAIN'] = -8   #vx 1/18

    @LO_RAILED_checker
    def run(self):
        #if not TTFSS_LOCKED():
        #    notify('TTFSS NOT LOCKED!')
        return True

class IDLE(GuardState):
    index = 3
    request = False

    @TTFSS_checker
    @LO_RAILED_checker
    def run(self):
        if not OPO_LOCKED():
            notify('OPO Unlocked')
            return False

        if ezca['SQZ-HD_DIFF_RF3_DEMOD_RFMON'] < -30:
           log('Check 3MHz RF level')
           return False

        return True

#    def run(self):
#        if ezca['SQZ-HD_DIFF_RF3_DEMOD_RFMON'] < -30:
#            log('Check 3MHz RF level')
#        if ezca['GRD-SQZ_PLL_STATE_N'] != 1:
#            log('PLL not DOWN')
#        if abs(ezca['SQZ-LO_SERVO_FASTMON']) == 10:
#            log('LO rails')
#            ezca['SQZ-LO_SERVO_COMBOOST']=0
#        return (ezca['SQZ-HD_DIFF_RF3_DEMOD_RFMON'] > -30) and (ezca['GRD-SQZ_PLL_STATE_N'] == 1) and (abs(ezca['SQZ-LO_SERVO_FASTMON']) < 10)


class ADJUST_FREQUENCY(GuardState):
    index = 4
    request = False

    def run(self):
        BEATNOTEave = cdu.avg(1,'H1:SQZ-FREQ_LASERBEATVSDOUBLELASERVCO', stddev = True)
        if abs(BEATNOTEave[0]) > 100:
            ezca['SQZ-LASER_VCO_CONTROLS_EXTERNAL'] = 0
            ezca['SQZ-LASER_VCO_CONTROLS_ENABLE'] = 1
            notify('Check 3MHz beat note, adjust 80MHz VCO TUNEOFS to optimize.')
        else:
            return True


################################################################################################
#LOCKING 3MHz with OMC
################################################################################################


class LOCKING_OMC(GuardState):
    index = 15
    request = False

    @TTFSS_checker
    def main(self):
        log('locking LO on OMC')
        self.timer['bdiv_timeout'] = 30

        log('DISABLE SLOWCONTROLS VCO SERVO')
        ezca['SQZ-LASER_VCO_CONTROLS_ENABLE'] = 0

        log('closing sqzt7 PSL-LO flipper')
        ezca['SQZ-LO_FLIPPER_CONTROL'] = 0

        self.counter=0

    @TTFSS_checker
    @LO_RAILED_checker
    def run(self):

        if self.counter == 0:
            #if beam diverter is open
            if ezca['SYS-MOTION_C_BDIV_E_POSITION']==0 and not ezca['SYS-MOTION_C_BDIV_E_BUSY']:
                self.counter += 1
            else:
                notify('Beam diverter is not open')
                if self.timer['bdiv_timeout']:
                    return 'DOWN'

        #check OMC_RF3 signal
        if self.counter == 1:
            if ezca['SQZ-OMC_TRANS_RF3_DEMOD_RFMON'] > -18:    #if RFave[0]>-20:
                notify('Got OMC 3MHz beatnote, locking OMC-LO')
                ezca['SQZ-LO_SERVO_IN1EN'] = 1
                if FC_LOCKED():
                    notify('Engaging LO BOOSTS, fc is locked')
                    ezca['SQZ-LO_SERVO_COMBOOST']=2
                    return True
                else: notify('waiting for FC to boost')
            else:
                notify('Beam diverter is open but no 3MHz on OMC, could be an alignment problem')

        ''' vx3/13 maybe we don't need the std-dev to be good to lock
        #if loop closed, fc locked, and stdev OK, boost
        if self.counter == 2:
            RFave = cdu.avg(0.5,'H1:SQZ-OMC_TRANS_RF3_DEMOD_RFMON', stddev = True)
            log('RF3 average 1s = {}'.format(RFave[0]))
            log('RF3 std 1s = {}'.format(RFave[1]))
            if RFave[1] < 1:
                if FC_LOCKED():
                    notify('Engaging LO BOOSTS, fc is locked')
                    ezca['SQZ-LO_SERVO_COMBOOST']=2
                    return True
                else: notify('waiting for FC to boost')
            else: notify('OMC_RF3 noisy, wait to boost')
        '''

        if ezca.read('GRD-SQZ_CLF_LR_STATE_S', as_string=True) != 'LOCKED':
            notify('CLF not locked')


class LOCKED_OMC(GuardState):
    index = 20

    @TTFSS_checker
    @LO_RAILED_checker
    def run(self):
        '''
        #look at RF3 rms
        RFave = cdu.avg(0.5,'H1:SQZ-OMC_TRANS_RF3_DEMOD_RFMON', stddev = True)
        #is this a good std dev value?
        if RFave[1] > 1:
            notify('LO OMC_RF3 too noisy')
            return 'DOWN'
        '''
        if not OPO_LOCKED():
           notify('OPO not locked')
           return 'DOWN'

        if ezca['SQZ-OMC_TRANS_RF3_DEMOD_RFMON'] < -22:
            notify('LOW OMC_RF3 power < -22! Align SQZ-OMC_RF3, or more CLF power if aligned.')

        if ezca.read('GRD-SQZ_CLF_LR_STATE_S', as_string=True) != 'LOCKED':
            notify('CLF not locked')

        return True


################################################################################################
#LOCKING 3MHz with Homodyne
################################################################################################

class LOCKING_HD(GuardState):
    index = 5
    request = False

    #TTFSS_checker
    def main(self):
        log('locking LO on Homodyne')
        #ezca['SQZ-LO_SERVO_IN2GAIN'] = 14   # for SQZ LASER VCO LOCKING
        #ezca['SQZ-LO_SERVO_IN2GAIN'] = 31   # for LO PZT LOCKING

        if not ezca['SQZ-HD_FLIPPER_READBACK']: #open HD flipper when locking LO on HD
            notify('HD PATH CLOSED, OPENING HD PATH')
            ezca['SQZ-HD_FLIPPER_CONTROL'] = 1
            time.sleep(1)
            ezca['SQZ-HD_FLIPPER_CONTROL'] = 0

        if not ezca['SQZ-LO_FLIPPER_READBACK']: #open LO flipper when locking LO on HD
            notify('LO FLIPPER CLOSED, OPENING')
            ezca['SQZ-LO_FLIPPER_CONTROL'] = 1

        self.counter=0

    #TTFSS_checker
    def run(self):
        if self.counter == 0:
            log('DISABLE SLOWCONTROLS VCO SERVO')
            ezca['SQZ-LASER_VCO_CONTROLS_ENABLE'] = 0

            if not ezca['SQZ-CLF_FLIPPER_READBACK']: #open HD flipper when locking LO on HD
                notify('CLF FLIPPER CLOSED ON SQZT0!')
            else:
                time.sleep(0.5)
                self.counter+=1

        #check HD_RF3 signal
        if self.counter == 1:
            if ezca['SQZ-HD_DIFF_RF3_DEMOD_RFMON'] < -40: #notify low LO power
                if ezca['SQZ-HD_A_DC_OUTPUT'] < 0.25:
                    notify('Low HD LO power')
                notify('Locking HD with low RF3 power')
            else:
                log('found HD_RF3, locking LO on HD')
                ezca['SQZ-LO_SERVO_IN2EN'] = 1
                time.sleep(1)
                self.counter += 1

        #if loop closed, fc locked, and stdev OK, boost
        if self.counter == 2:
            RFave = cdu.avg(1,'H1:SQZ-HD_DIFF_RF3_DEMOD_RFMON', stddev = True)
            log('RF3 average 1s = {}'.format(RFave[0]))
            log('RF3 std 1s = {}'.format(RFave[1]))
            return True

            if RFave[1] < 0.35:
                if FC_LOCKED():
                    notify('Engaging LO BOOSTS, fc is locked')
                    ezca['SQZ-LO_SERVO_COMBOOST']=2
                    return True
                else: notify('waiting for FC to boost')
            else: notify('HD_RF3 noisy, wait to boost')

        if not ezca['SQZ-HD_FLIPPER_READBACK']:
            notify('HD LO Flipper closed on SQZT7.')

        if LO_RAILED():
            notify('LO railed!')

        if ezca.read('GRD-SQZ_CLF_LR_STATE_S', as_string=True) != 'LOCKED':
            notify('CLF not locked')



class LOCKED_HD(GuardState):
    index = 10

    #@TTFSS_checker
    def run(self):

        if not OPO_LOCKED():
           notify('OPO not locked')
           return 'DOWN'

        if LO_RAILED():
           notify('LO railing!')
           return 'DOWN'

        if ezca['SQZ-HD_DIFF_RF3_DEMOD_RFMON'] < -50:
            log('No 3MHz signal')
            return 'DOWN'

        if ezca['SQZ-HD_DIFF_RF3_DEMOD_RFMON'] < -40:
            notify('Locked LO HD with low RF3 power < -40')

        if ezca.read('GRD-SQZ_CLF_LR_STATE_S', as_string=True) != 'LOCKED':
            notify('CLF not locked')

        return True

        '''
        #RFave = cdu.avg(1,'H1:SQZ-HD_DIFF_RF3_DEMOD_RFMON', stddev = True)
        #log('RF3 average 1s = {}'.format(RFave[0]))
        #log('RF3 std 1s = {}'.format(RFave[1]))
        #not sure this logic is necessary? -Lee
        #should perhaps check if board has railed instead
        if RFave[1] < -10:
            log(ezca['SQZ-HD_DIFF_RF3_DEMOD_RFMON'])
            log(RFave[1])
            log('LO Not locked')
            return 'DOWN'
        '''

##################################################

edges = [
    ('INIT', 'DOWN'),
    ('IDLE', 'DOWN'),
    ('DOWN', 'DOWN'),
    ('DOWN', 'LOCKING_HD'),
    ('LOCKING_HD', 'LOCKED_HD'),
    ('DOWN','ADJUST_FREQUENCY'),
    ('DOWN', 'LOCKING_OMC'),
    ('LOCKING_OMC', 'LOCKED_OMC')]


